####
# Before building the docker image run:
#
# mvn package -Pnative -Dnative-image.docker-build=true
#
# Then, build the image with:
#
# docker build -f src/main/docker/Dockerfile -t shamrock/ratings-quarkus .
#
# Then run the container using:
#
# docker run -i --rm -p 8080:8080 shamrock/ratings-quarkus
#
###
#FROM registry.fedoraproject.org/fedora-minimal
FROM cescoffier/native-base:latest
#FROM gcr.io/distroless/base   --- lacks libz.so
WORKDIR /
EXPOSE 9080
LABEL io.kiali.runtime=quarkus
CMD ["/application", "-Dquarkus.http.host=0.0.0.0", "-Dquarkus.http.port=9080", "-Xms1m", "-Xmx4m", "-Xmn4m" ]
COPY *-runner /application
