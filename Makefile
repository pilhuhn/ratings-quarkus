
##GRAALVM_HOME=/Library/Java/JavaVirtualMachines/graalvm-ce-19.0.2/Contents/Home/ 

help:
	echo "Usage make [dev|jar|native|crossc|docker|docker-run]"
	exit 1;

dev:
	mvn clean compile quarkus:dev

jar:
	mvn clean package
	java -jar target/ratings-quarkus-*-runner.jar

native:
	mvn clean -Pnative package

crossc:
	mvn clean -Pnative package -Dnative-image.docker-build=true

docker: crossc
	docker build -t ratings-jug -f src/main/docker/Dockerfile target/

docker-run:
	docker run ratings-jug




