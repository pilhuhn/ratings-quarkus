/*
 * Copyright 2019 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.istio.ratings;

import static de.bsd.istio.ratings.TraceUtil.getTraceId;

import io.soabase.structured.logger.formatting.Arguments;
import io.soabase.structured.logger.formatting.LevelLogger;
import io.soabase.structured.logger.formatting.LoggingFormatter;
import io.soabase.structured.logger.formatting.gelf.ExceptionFormatter;
import io.soabase.structured.logger.formatting.gelf.ExceptionFormatterImpl;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;

/**
 * @author hrupp
 */
public class LocalLokiLoggingFormatter implements LoggingFormatter {
  private Label[] labels;
  private ExceptionFormatter exceptionFormatter;

  LocalLokiLoggingFormatter(Label... labels) throws RuntimeException {
    this.labels = labels;
    this.exceptionFormatter = new ExceptionFormatterImpl(20);

  }

  @Override
  public void apply(LevelLogger levelLogger, Logger logger, List<String> schemaNames, Arguments arguments, String msg,
                    Throwable throwable) {


    StringBuilder sb = new StringBuilder();


    fillUserData(schemaNames, arguments, throwable, sb, exceptionFormatter);

    sb.append(" traceId=").append(getTraceId());

    sb.append(" msg=\"").append(msg).append('"');

    if (labels!=null && labels.length>0) {
      String labelString = formatLabels();
      sb.append(' ').append(labelString);
    }

    log(levelLogger,logger, sb.toString());
  }

  static void fillUserData(List<String> schemaNames, Arguments arguments, Throwable throwable, StringBuilder sb, ExceptionFormatter exceptionFormatter) {
    Iterator<String> schemas = schemaNames.iterator();
    int index = 0;

    while (schemas.hasNext()) {
      String name = schemas.next();
      sb.append(" ");
      sb.append("_").append(name).append("=").append(arguments.get(index++));
    }

    if (throwable!=null) {
      sb.append(" ");
      sb.append("_exception=\"").append(exceptionFormatter.format(throwable)).append("\"");
    }
  }

  private String formatLabels() {
    StringBuilder sb = new StringBuilder();
    for (int i = 0 ; i < labels.length ; i++) {
      sb.append(labels[i].toString());
      if (i < labels.length-1) {
        sb.append(' ');
      }
    }

    return sb.toString();
  }


  void log(LevelLogger levelLogger, Logger logger, String messageLine) {

    levelLogger.log(logger,messageLine);


  }
}
