/*
 * Copyright 2019 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.istio.ratings;

import io.soabase.structured.logger.StructuredLogger;
import io.soabase.structured.logger.StructuredLoggerFactory;
import io.soabase.structured.logger.formatting.gelf.SimpleJsonBuilder;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.opentracing.Traced;

/**
 * @author hrupp
 */
@Path("/ratings")
public class  RatingsEndpoint {


  private static StructuredLogger<LoggingSchema> flog = StructuredLoggerFactory.getLogger(LoggingSchema.class,
           new JsonLoggingFormatter(new SimpleJsonBuilder(),true, new Label("app","ratings"), new Label("version",
                                                                                                        "v1")));
  //      new RemoteLokiLoggingFormatter("localhost", 3100, new JacksonJsonBuilder(new ObjectMapper()),
  //                                     new Label("app","ratings"),
  //                                     new Label("version","v1")));
//      new LocalLokiLoggingFormatter(new Label("app","ratings"),
//                                             new Label("version","v2")));

  private static final CachedRatings cachedRatings = new CachedRatings();

  @GET
  public String hello() {
    return "Hello Stuttgart\n";
  }

  @Traced
  @GET
  @Path("/{bookId}")
  @Produces("application/json")
  @Counted
  public Map<String,?> getRating(@PathParam("bookId") int bookId) {


    int rating1 = cachedRatings.getRating();


/*
   Ratings rfd = Ratings.getRating((long)bookId);
   if (rfd != null) {
     rating1 = rfd.rating1;
   }
*/


   if (Math.random()>0.85) {
     flog.error("Now with Exception",new IllegalArgumentException("Just a dummy"),
                loggingSchema -> loggingSchema.bookId(bookId));
   } else {
     flog.info("Endpoint",loggingSchema -> loggingSchema.bookId(bookId).rev1(rating1).rev2(4));
   }

   JsonObject rateMap = Json.createObjectBuilder()
       .add("Reviewer1",rating1)
       .add("Reviewer2",4)
       .build();

    JsonObject rating = Json.createObjectBuilder()
        .add("id",bookId)
        .add("ratings", rateMap)
        .build();

    return rating;
  }
}
