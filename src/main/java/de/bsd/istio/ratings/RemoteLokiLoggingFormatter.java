/*
 * Copyright 2019 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.istio.ratings;

import static de.bsd.istio.ratings.TraceUtil.getTraceId;

import io.soabase.structured.logger.formatting.Arguments;
import io.soabase.structured.logger.formatting.LevelLogger;
import io.soabase.structured.logger.formatting.LoggingFormatter;
import io.soabase.structured.logger.formatting.gelf.ExceptionFormatter;
import io.soabase.structured.logger.formatting.gelf.ExceptionFormatterImpl;
import io.soabase.structured.logger.formatting.gelf.JsonBuilder;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;

/**
 * @author hrupp
 */
public class RemoteLokiLoggingFormatter implements LoggingFormatter {
  private final JsonBuilder jsonBuilder;
  private Label[] labels;
  private String postUrl;
  private DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
  private ExceptionFormatter exceptionFormatter;

  RemoteLokiLoggingFormatter(String host, int port, JsonBuilder jsonBuilder, Label... labels) throws RuntimeException {
    this.jsonBuilder = jsonBuilder;
    this.labels = labels;
    postUrl = "http://" + host + ":" + port + "/api/prom/push";
    this.exceptionFormatter = new ExceptionFormatterImpl(20);

  }

  @Override
  public void apply(LevelLogger levelLogger, Logger logger, List<String> schemaNames, Arguments arguments, String msg,
                    Throwable throwable) {

    Map<String,Object> logLine = new HashMap<>();

    StringBuilder sb = new StringBuilder();
    sb.append(msg);


    LocalLokiLoggingFormatter.fillUserData(schemaNames, arguments, throwable, sb, exceptionFormatter);

    sb.append(" level=").append(levelLogger.getLevel().toString());
    sb.append(" traceId=").append(getTraceId());

    logLine.put("line",sb.toString());
    addStandardFields(logLine);
    logLine.put("level",levelLogger.getLevel().toString());

    List<Map> entries = new ArrayList<>(1);
    entries.add(logLine);

    Map<String,Object> streamsMap = new HashMap<>();
    streamsMap.put("entries",entries);
    if (labels!=null && labels.length>0) {
      String labelString = formatLabels();
      streamsMap.put("labels",labelString);
    }


    List<Map> entriesArray = new ArrayList<>(1);
    entriesArray.add(streamsMap);

    JsonBuilder b = jsonBuilder;
    Object o = b.newObject();
    b.addField(o,"streams",entriesArray);

    log(levelLogger,logger,b.finalizeToJson(o));
  }

  private String formatLabels() {
    StringBuilder sb = new StringBuilder("{");
    for (int i = 0 ; i < labels.length ; i++) {
      sb.append(labels[i].toString());
      if (i < labels.length-1) {
        sb.append(',');
      }
    }
    sb.append('}');

    return sb.toString();
  }

  private void addStandardFields(Map<String, Object> logLine) {
    logLine.put("ts", df.format(new Date()));
    logLine.put("traceId",getTraceId());
  }

  void log(LevelLogger levelLogger, Logger logger, String json) {

    HttpURLConnection urlConnection;
    try {
      urlConnection = (HttpURLConnection) new URL(postUrl).openConnection();
      urlConnection.setRequestMethod("POST");
      urlConnection.setRequestProperty("Content-Type", "application/json");
      urlConnection.setDoOutput(true);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }


    try (OutputStream output = urlConnection.getOutputStream()) {
      try {
        output.write(json.getBytes(StandardCharsets.UTF_8));
        output.flush();
        System.out.println(urlConnection.getResponseCode() + " " + urlConnection.getResponseMessage());
      } catch (IOException e) {
        logger.error("Write to Loki failed:",e);
      }
    } catch (IOException e) {
      logger.error("Connection to Loki failed:",e);
    }

  }
}
