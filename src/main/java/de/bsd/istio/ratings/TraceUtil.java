/*
 * Copyright 2019 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.istio.ratings;

import io.jaegertracing.internal.JaegerSpanContext;
import io.opentracing.Span;
import io.opentracing.SpanContext;
import io.opentracing.Tracer;
import io.opentracing.util.GlobalTracer;

/**
 * @author hrupp
 */
public class TraceUtil {

  static String getTraceId() {
    Tracer gt = GlobalTracer.get();
    Span sp = gt.activeSpan();
    SpanContext ctx = sp.context();
    if (ctx instanceof JaegerSpanContext) {
      JaegerSpanContext jsc = (JaegerSpanContext) ctx;
      return jsc.getTraceId();
    }
    return "dummy-" + ctx.toString(); // TODO this is a dummy
  }

}
