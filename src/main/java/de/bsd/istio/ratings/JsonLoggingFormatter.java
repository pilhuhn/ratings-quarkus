/*
 * Copyright 2019 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.istio.ratings;

import static de.bsd.istio.ratings.TraceUtil.getTraceId;

import io.soabase.structured.logger.formatting.Arguments;
import io.soabase.structured.logger.formatting.LevelLogger;
import io.soabase.structured.logger.formatting.LoggingFormatter;
import io.soabase.structured.logger.formatting.gelf.JsonBuilder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;

/**
 * @author hrupp
 */
public class JsonLoggingFormatter implements LoggingFormatter {

  private final JsonBuilder jsonBuilder;
  /** If this is true, we don't use the logger to write the output, but write the json to stdout */
  private boolean jsonOnly;
  private Label[] labels;
  private DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

  public JsonLoggingFormatter(JsonBuilder jsonBuilder, Label... labels) {
    this.jsonBuilder = jsonBuilder;
    this.labels = labels;
    this.jsonOnly = false;
  }

  public JsonLoggingFormatter(JsonBuilder jsonBuilder, boolean jsonOnly, Label... labels) {
    this.jsonBuilder = jsonBuilder;
    this.jsonOnly = jsonOnly;
    this.labels = labels;
  }

  @Override
  public void apply(LevelLogger levelLogger, Logger logger, List<String> schemaNames, Arguments arguments, String s,
                    Throwable throwable) {
    Object o = this.jsonBuilder.newObject();

    this.addStandardFields(o,s );

    Iterator<String> schemas = schemaNames.iterator();
    int index = 0;

    while (schemas.hasNext()) {
      String name = schemas.next();
      this.jsonBuilder.addField(o, "_" + name , arguments.get(index++));
    }


    if (jsonOnly) {
      this.jsonBuilder.addField(o, "level", levelLogger.getLevel().toString().toLowerCase());
      this.jsonBuilder.addField(o, "thread-id", Thread.currentThread().getName());
      this.jsonBuilder.addField(o,"ts", df.format(new Date()));
    }

    if (labels!=null && labels.length>0) {
      for (Label label : labels) {
        jsonBuilder.addField(o, label.getKey(),label.getValue());
      }
    }


    if (throwable!=null) {
      this.jsonBuilder.addExceptionField(o, throwable);
    }

    String json = this.jsonBuilder.finalizeToJson(o);
    this.log(levelLogger,logger,json);
  }

  private void addStandardFields(Object o, String mainMessage) {
    this.jsonBuilder.addField(o,"msg",mainMessage);

    // TODO add more fields
    this.jsonBuilder.addField(o, "traceId", getTraceId());

  }


  void log(LevelLogger levelLogger, Logger logger, String json) {
    if (jsonOnly) {
      System.out.println(json);
    } else {
      levelLogger.log(logger, json);
    }
  }


}
